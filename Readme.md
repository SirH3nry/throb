Throb
============
A Direct3D device that supports the XNA Framework HiDef profile is required.
This game is played using either keyboard or X-Box 360 controller 

You may be required to download the following or newer:
.NET Framework 2.0
DirectX 9.0c
XNA Framework

Now that your computer is up to date:
1. Download ThrobReleaseV2.zip
2. Run the setup.exe file
3. Complete the installation
4. Enjoy Throb!

-----------------------------------------------------------------------------
Alexander Yochim: Sound and Scripting

Shane Shafferman: Scripting Lead

Peter Kalmar: Art

Kevin Plansinis: Scripting

All music in this game was either made by the team or is licensed by Creative Commons, used under permission.

-----------------------------------------------------------------------------
Developed during Global Game Jam 2013, the theme was the sound of a heartbeat. Using a keyboard or gamepad, the player must match the beat of a song while also matching the character's heartbeat. The heartbeat fluctuates depending on how well the player is matching the song beat.