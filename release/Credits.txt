Alexander Yochim: Sound and Scripting

Shane Shafferman: Scripting Lead

Peter Kalmar: Art

Kevin Plansinis: Scripting

All music in this game was either made by the team or is licensed by Creative Commons, used under permission.