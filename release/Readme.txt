A Direct3D device that supports the XNA Framework HiDef profile is required.
This game is played using either keyboard or X-Box 360 controller 

You may be required to download the following or newer:
.NET Framework 2.0
DirectX 9.0c
XNA Framework

Now that your computer is up to date:
1. Download ThrobReleaseV2.zip
2. Run the setup.exe file
3. Complete the installation
4. Enjoy Throb!