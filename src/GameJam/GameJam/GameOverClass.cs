﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class GameOverClass
    {
        #region Image Vars

        private Texture2D rafters;
        private Texture2D stage;
        private Texture2D heart;

        #endregion

        #region String Values
        private String s1 = "Total Score: ";
        private String s2 = "Levels Completed: ";
        private String s3 = "Rating: ";
        private String disp1 = "";
        private String disp2 = "";
        #endregion

        private int rating = 0;

        public GameOverClass() { }

        public void Initialize(GraphicsDevice gd)
        {

        }

        public void LoadContent(ContentManager c)
        {
            stage = c.Load<Texture2D>("Images/drumSetandStage");
            rafters = c.Load<Texture2D>("Images/rafters");
            heart = c.Load<Texture2D>("Images/ratingHeart");
        }

        public void Unload()
        {

        }

        public void Update(GameTime gt)
        {
            Lib.input.Update();

            disp1 = s1 + Lib.score.ToString();

            #region HeartRating
            if (Lib.score < 5000)
            {
                rating = 0;
            }
            else if (Lib.score < 150000)
            {
                rating = 1;
            }
            else if (Lib.score < 3000000)
            {
                rating = 2;
            }
            else if (Lib.score < 50000000)
            {
                rating = 3;
            }
            else if (Lib.score < 900000000)
            {
                rating = 4;
            }
            else if (Lib.score < 12000000000)
            {
                rating = 5;
            }
            else if (Lib.score < 150000000000)
            {
                rating = 6;
            }
            else
            {
                rating = 7;
            }
            #endregion

#if WINDOWS
            if ((Lib.input.IsNewPress(Keys.Space)) || (Lib.input.IsNewPress(Buttons.Start)))
            {
                Game1.thisGame.reset();
                Lib.gameState = Lib.GameState.Start_Menu;
            }
#endif

#if XBOX360
                //insert 360 code here.  
#endif
        }

        public void Draw(SpriteBatch sb, GraphicsDevice gd)
        {
            sb.Begin();
            if (Lib.gameOverType)
            {
                sb.Draw(Lib.background, Vector2.Zero, Color.White);
                sb.Draw(rafters, Vector2.Zero, Color.White);
                sb.DrawString(Lib.gameFont, "Song Completed!", new Vector2(200, 30), Color.White);
                sb.DrawString(Lib.gameFont, disp1, new Vector2(250, 80), Color.White);
                sb.DrawString(Lib.gameFont, disp2, new Vector2(250, 130), Color.White);
                sb.DrawString(Lib.gameFont, s3, new Vector2(250, 180), Color.White);

                for (int i = 0; i < rating; i++)
                {
                    sb.Draw(heart, new Vector2(315 + (16 * i), 185), Color.White);
                }
            }
            else
            {
                sb.Draw(Lib.background, Vector2.Zero, Color.White);
                sb.Draw(rafters, Vector2.Zero, Color.White);
                sb.DrawString(Lib.gameFont, "Game Over...", new Vector2(200, 30), Color.White);
                sb.DrawString(Lib.gameFont, disp1, new Vector2(250, 80), Color.White);
                sb.DrawString(Lib.gameFont, disp2, new Vector2(250, 130), Color.White);
            }
            sb.DrawString(Lib.gameFont, "Press 'Space' or 'Start' to continue.", new Vector2(300, 230), Color.White);

            sb.End();
        }
    }
}