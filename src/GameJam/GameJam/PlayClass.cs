﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class PlayClass
    {
        #region Image Vars
        public Effect pauseFilter;
        public RenderTarget2D light;
        public RenderTarget2D scene;
        private Texture2D rhythmBar;
        private Texture2D rhythmLogo;
        private Texture2D[] spotlights = new Texture2D[9];
        private Texture2D stage;
        private Texture2D rafters;
        private Texture2D over;
        public Person[] crowd = new Person[2];
        private Texture2D[] flash = new Texture2D[2];
        public List<Person> persons = new List<Person>();
        public Texture2D[] waves = new Texture2D[6];

        //timer vars for flash
        public double flashCountLeft = 0.0;
        public double flashTimerLeft = 0.3;

        public double flashCountRight = 0.0;
        public double flashTimerRight = 0.3;

        //light timer
        public float lightCounter = 0.0f;
        public float lightTime = 2.0f;
        public float[] lightRotate = new float[9];

        #endregion

        #region Movement
        //y movement (dancing)
        private Random rand = new Random();
        private double bounce = 0;
        //x movement (shifting)
        private int shift;
        private double shiftTime;
        private double shiftCounter = 0;
        //runner shifting
        private List<double> runnerCounter = new List<double>();
        #endregion

        #region Rhythm Vars
        //Right Rhythm Vars
        //button spawn timer
        private double spawnTimeRight = 1.5;
        private double spawnCountRight = 0.0;
        //scroll across the bar
        private List<ScrollButton> btnsRight = new List<ScrollButton>();
        private List<ScrollButton> missedRight = new List<ScrollButton>();
        //show spark effect
        private bool hitRight = false;

        //left rhythm vars
        //button spawn timer
        private double spawnTimeLeft = 1.0;
        private double spawnCountLeft = 0.0;
        //scroll across the bar
        private List<ScrollButton> btnsLeft = new List<ScrollButton>();
        private List<ScrollButton> missedLeft = new List<ScrollButton>();
        //show spark effect
        private bool hitLeft = false;

        //heart rate vars
        private int heartCombo = 0;
        private int musicCombo = 0;

        private bool activeWave = false;
        private int activeWaveStep = 1;

        #endregion

        private int crowdSize;
        private MusicClass mc;
        private bool startMusic = true;

        #region Knockback
        private double spd;
        private int mass;
        private int t; // texture index
        private int spawnCount = 4;
        private int kb;
        private int kbCount = 0;
        #endregion

        public PlayClass(MusicClass mc)
        {
            this.mc = mc;
            crowdSize = 10;
        }

        public void Initialize(GraphicsDevice gd)
        {
            light = new RenderTarget2D(gd, 800, 350);
            scene = new RenderTarget2D(gd, 800, 350);

            shift = 5;
            shiftTime = 1;
        }

        public void LoadContent(ContentManager c)
        {
            spawnTimeRight = mc.calcBeatsS(Lib.trackSelected);
            //rhythmBar
            rhythmBar = c.Load<Texture2D>("Images/BeatBarHeart");
            rhythmLogo = c.Load<Texture2D>("Images/BeatBar_LOGO");
            //load wave images
            waves[0] = c.Load<Texture2D>("Images/Waves/wave1");
            waves[1] = c.Load<Texture2D>("Images/Waves/wave2");
            waves[2] = c.Load<Texture2D>("Images/Waves/wave3");
            waves[3] = c.Load<Texture2D>("Images/Waves/wave4");
            waves[4] = c.Load<Texture2D>("Images/Waves/wave5");
            waves[5] = c.Load<Texture2D>("Images/Waves/wave6");

            //load rhythm textures
            Lib.buttonCue[0] = c.Load<Texture2D>("Images/nodeHeart");
            Lib.buttonCue[1] = c.Load<Texture2D>("Images/nodeRight");
            //load flash textures
            flash[0] = c.Load<Texture2D>("Images/hitHeart");
            flash[1] = c.Load<Texture2D>("Images/hitRight");

            //load people textures
            Lib.people[0] = c.Load<Texture2D>("Images/runner1");
            Lib.people[1] = c.Load<Texture2D>("Images/runner2");
            Lib.people[2] = c.Load<Texture2D>("Images/runner4");
            Lib.people[3] = c.Load<Texture2D>("Images/runner5");

            //load stage
            stage = c.Load<Texture2D>("Images/drumSetandStage");
            rafters = c.Load<Texture2D>("Images/rafters");
            pauseFilter = c.Load<Effect>("PauseShader");
            spotlights[0] = c.Load<Texture2D>("Images/Light/spotLightBlue");
            spotlights[1] = c.Load<Texture2D>("Images/Light/spotLightGrey");
            spotlights[2] = c.Load<Texture2D>("Images/Light/spotLightYellow");
            spotlights[3] = c.Load<Texture2D>("Images/Light/spotLightGreen");
            spotlights[4] = c.Load<Texture2D>("Images/Light/spotLightOrange");
            spotlights[5] = c.Load<Texture2D>("Images/Light/spotLightPink");
            spotlights[6] = c.Load<Texture2D>("Images/Light/spotLightPurple");
            spotlights[7] = c.Load<Texture2D>("Images/Light/spotLightRed");
            spotlights[8] = c.Load<Texture2D>("Images/Light/spotLightTeal");
            over = c.Load<Texture2D>("Images/GameOver");
            //init back of the crowd
            crowd[0] = new Person();
            crowd[0].Image = c.Load<Texture2D>("Images/crowdBack");
            crowd[0].xPosition = -425;
            crowd[0].yPosition = 125;
            crowd[0].Speed = 0.5;
            crowd[0].Mass = 100;
            //init front of the crowd
            crowd[1] = new Person();
            crowd[1].Image = c.Load<Texture2D>("Images/crowdFront");
            crowd[1].xPosition = -500;
            crowd[1].yPosition = 0;
            crowd[1].Speed = 1.0;
            crowd[1].Mass = 90;

            for (int i = 0; i < persons.Count; i++)
            {
                mass = rand.Next(1, 11);
                persons.Add(new Person(spd, mass));
                t = rand.Next(0, 4);
                persons[i].Image = Lib.people[t];
                persons[i].xPosition = 10;
                persons[i].yPosition = rand.Next(230, 270);
            }
        }

        public void Unload()
        {

        }

        public void Update(GameTime gt)
        {
            Lib.input.Update();
            if (!Lib.pause)
            {
                #region Bouncing Crowd
                //randomly bounces the crowd up and down
                bounce += gt.ElapsedGameTime.TotalMilliseconds;
                int r = rand.Next(100, 1000);
                if (bounce >= r)
                {
                    r = rand.Next(0, 2);
                    bounceCrowd(crowd[r]);
                    bounce = 0;
                }
                #endregion

                #region Move Sideways
                shiftCounter += gt.ElapsedGameTime.TotalSeconds;
                //moves the crowd right based on difficulty
                if (shiftCounter >= shiftTime)
                {
                    spawnCount--;
                    shiftCounter = 0;
                    crowd[0].xPosition += shift;
                    crowd[1].xPosition += shift;

                    if (spawnCount == 0)
                    {
                        spawnCount = 4;
                        spd = (1 + rand.NextDouble() * 5) / 7;
                        mass = rand.Next(1, 11);
                        persons.Add(new Person(spd, mass));
                        runnerCounter.Add(0.0);
                        int i = persons.Count - 1; //get newest position
                        t = rand.Next(0, 4);
                        persons[i].Image = Lib.people[t];
                        persons[i].xPosition = 10;
                        persons[i].yPosition = rand.Next(230, 270);

                        crowdSize++;
                    }
                }

                //move runners
                for (int y = 0; y < persons.Count; y++)
                {
                    runnerCounter[y] += gt.ElapsedGameTime.TotalSeconds;
                    if (runnerCounter[y] >= persons[y].Speed)
                    {
                        runnerCounter[y] = 0;
                        persons[y].xPosition += 10;
                    }
                    if (persons[y].xPosition + 100 >= 725)
                    {
                        //game over
                        Lib.pause = true;
                        Lib.lost = true;
                    }
                }

                #endregion

                #region Rhythm Handling RIGHT

                for (int v = 0; v < btnsRight.Count; v++)
                {
                    btnsRight[v].scrollCount += gt.ElapsedGameTime.TotalSeconds;
                    if (btnsRight[v].scrollCount < btnsRight[v].scrollTime)
                    {
                        btnsRight[v].pos.X -= 1;
                        if (btnsRight[v].pos.X < 457)
                        {
                            missedRight.Add(btnsRight[v]);
                            btnsRight.RemoveAt(v);
                        }
                    }
                }
#if WINDOWS
                if (btnsRight.Count > 0)
                {
                    if ((Lib.input.IsNewPress(Keys.E)) || (Lib.input.IsNewPress(Buttons.RightTrigger)))
                    {
                        if ((btnsRight[0].pos.X >= 458) && (btnsRight[0].pos.X <= 482))
                        {
                            mc.playSoundFx();
                            hitRight = true;
                            Lib.score += (ulong)(2137 * Lib.multiplier);
                            musicCombo++;
                            btnsRight.RemoveAt(0);
                            //push back
                            pushBack();
                        }
                    }
#endif
#if XBOX360
                //insert 360 code here.  
#endif
                }

                #region Early Hit Right
                if (btnsRight.Count > 0)
                {
#if WINDOWS
                    if ((Lib.input.IsNewPress(Keys.E)) || (Lib.input.IsNewPress(Buttons.RightTrigger)))
                    {
                        if ((btnsRight[0].pos.X > 480) && !hitRight)
                        {
                            musicCombo = 0;
                            Lib.multiplier = 1;
                            Lib.heartRate = 60;
                            //play noise
                        }
                    }
#endif
#if XBOX360
                //insert 360 code here.  
#endif
                }
                #endregion

                for (int m = 0; m < missedRight.Count; m++)
                {
                    missedRight[m].scrollCount += gt.ElapsedGameTime.TotalSeconds;
                    if (missedRight[m].scrollCount < missedRight[m].scrollTime)
                    {
                        missedRight[m].pos.X -= 1;
                    }
                    else
                    {
                        missedRight.RemoveAt(m);
                        musicCombo = 0;
                        Lib.multiplier = 1;
                        Lib.heartRate = 60;
                    }
                }

                spawnCountRight += gt.ElapsedGameTime.TotalMilliseconds;
                if (spawnCountRight >= (1000 / spawnTimeRight))
                {
                    if (startMusic)
                    {
                        startMusic = false;
                        mc.playTrack(Lib.trackSelected, false);
                    }
                    spawnCountRight = 0.0;
                    btnsRight.Add(new ScrollButton(true));
                }

                #endregion

                #region Rhythm Handling LEFT

                spawnCountLeft += gt.ElapsedGameTime.TotalMilliseconds;
                if (spawnCountLeft >= (1000 / spawnTimeLeft))
                {
                    spawnCountLeft = 0.0;
                    btnsLeft.Add(new ScrollButton(false));
                }

                for (int v = 0; v < btnsLeft.Count; v++)
                {
                    btnsLeft[v].scrollCount += gt.ElapsedGameTime.TotalSeconds;
                    if (btnsLeft[v].scrollCount < btnsLeft[v].scrollTime)
                    {
                        btnsLeft[v].pos.X += 1;
                        if (btnsLeft[v].pos.X > 330)
                        {
                            missedLeft.Add(btnsLeft[v]);
                            btnsLeft.RemoveAt(v);
                        }
                    }
                }

                if (btnsLeft.Count > 0)
                {
#if WINDOWS
                        if ((Lib.input.IsNewPress(Keys.Q)) || (Lib.input.IsNewPress(Buttons.LeftTrigger)))
                        {
                            if ((btnsLeft[0].pos.X >= 290) && (btnsLeft[0].pos.X <= 314))
                            {
                                mc.playSoundFx();
                                hitLeft = true;
                                Lib.score += (ulong)(2137 * Lib.multiplier);
                                heartCombo++;
                                btnsLeft.RemoveAt(0);
                                //push back
                                pushBack();
                            }
                        }
#endif
#if XBOX360
                //insert 360 code here.  
#endif
                }

                #region Early Hit Left
                if (btnsLeft.Count > 0)
                {
#if WINDOWS
                        if ((Lib.input.IsNewPress(Keys.Q)) || (Lib.input.IsNewPress(Buttons.LeftTrigger)))
                        {
                            if ((btnsLeft[0].pos.X < 275) && !hitLeft)
                            {
                                heartCombo = 0;
                                Lib.multiplier = 1;
                                Lib.heartRate = 60;
                                //play noise
                            }
                        }
#endif
#if XBOX360
                //insert 360 code here.  
#endif
                }
                #endregion

                for (int m = 0; m < missedLeft.Count; m++)
                {
                    missedLeft[m].scrollCount += gt.ElapsedGameTime.TotalSeconds;
                    if (missedLeft[m].scrollCount < missedLeft[m].scrollTime-0.3)
                    {
                        missedLeft[m].pos.X += 1;
                    }
                    else
                    {
                        missedLeft.RemoveAt(m);
                    }
                }

                //spawnCountLeft += gt.ElapsedGameTime.TotalMilliseconds;
                //if (spawnCountLeft >= (1000/spawnTimeLeft))
                //{
                //    spawnCountLeft = 0.0;
                //    btnsLeft.Add(new ScrollButton(false));
                //}

                #endregion

                #region Multiplier Code
                spawnTimeLeft = Lib.heartRate / 60.0;
                if (musicCombo == 3)
                {
                    if (Lib.multiplier < 6)
                    {
                        Lib.multiplier++;
                        musicCombo = 0;
                    }
                    if (Lib.heartRate != mc.getBPM(Lib.trackSelected))
                    {
                        Lib.heartRate += 10;
                        if (Lib.heartRate > mc.getBPM(Lib.trackSelected))
                        {
                            Lib.heartRate = mc.getBPM(Lib.trackSelected);
                        }
                    }
                }
                #endregion

                #region Trigger Flash
                if (hitRight)
                {
                    activeWave = true;
                    flashCountRight += gt.ElapsedGameTime.TotalSeconds;
                    if (flashCountRight >= flashTimerRight)
                    {
                        flashCountRight = 0.0;

                        hitRight = false;
                    }
                }
                if (hitLeft)
                {
                    activeWave = true;
                    flashCountLeft += gt.ElapsedGameTime.TotalSeconds;
                    if (flashCountLeft >= flashTimerLeft)
                    {
                        flashCountLeft = 0.0;
                        hitLeft = false;
                    }
                }
                #endregion
            }
            if (!startMusic)
            {
                if (mc.getEndSong() == MediaState.Stopped)
                {
                    Lib.gameOverType = true;
                }
            }

            #region Pause
#if WINDOWS
                if ((Lib.input.IsNewPress(Keys.Space)) || (Lib.input.IsNewPress(Buttons.Start)))
                {
                    if (Lib.pause)
                    {
                        Lib.pause = false;
                        mc.ResumeTrack();
                    }
                    else
                    {
                        Lib.pause = true;
                        mc.PauseTrack();
                    }
#endif

#if XBOX360
                //insert 360 code here.  
#endif
            }

            #endregion

            #region Game Over
            if (Lib.lost)
            {
                Lib.deadCount += gt.ElapsedGameTime.TotalSeconds;
                if (Lib.deadCount >= Lib.deadTimer)
                {
                    Lib.gameOverType = false;
                    Lib.gameState = Lib.GameState.Game_Over;
                }
            }
            if (Lib.gameOverType)
            {
                Lib.gameState = Lib.GameState.Game_Over;
            }

            #endregion

            #region light
            if (!Lib.pause)
            {
                lightCounter += (float)gt.ElapsedGameTime.TotalSeconds;
                if (lightCounter >= lightTime)
                {
                    lightCounter = 0.0f;
                    for (int i = 0; i < 9; i++)
                    {
                        lightRotate[i] = (float)(rand.Next(-45, 45) * (Math.PI / 180));
                    }
                }
            }
            #endregion
        }

        public void Draw(SpriteBatch sb, GraphicsDevice gd)
        {
            if (Lib.pause)
            {
                gd.SetRenderTarget(light);

                gd.Clear(new Color(70, 70, 70));
                sb.Begin(SpriteSortMode.Immediate, BlendState.Additive);
                sb.Draw(spotlights[1], new Rectangle(575, -10, spotlights[1].Width, spotlights[1].Height),
                    null, Color.White, -0.4f, Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[1], new Rectangle(700, -10, spotlights[1].Width, spotlights[1].Height),
                    null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[0], new Rectangle(100, -10, spotlights[0].Width, spotlights[0].Height),
                    null, Color.White, lightRotate[0], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[2], new Rectangle(200, -10, spotlights[0].Width, spotlights[2].Height),
                    null, Color.White, lightRotate[2], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[3], new Rectangle(50, -10, spotlights[0].Width, spotlights[3].Height),
                    null, Color.White, lightRotate[3], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[4], new Rectangle(475, -10, spotlights[0].Width, spotlights[4].Height),
                    null, Color.White, lightRotate[4], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[5], new Rectangle(325, -10, spotlights[0].Width, spotlights[5].Height),
                    null, Color.White, lightRotate[5], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[6], new Rectangle(550, -10, spotlights[0].Width, spotlights[6].Height),
                    null, Color.White, lightRotate[6], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[7], new Rectangle(400, -10, spotlights[0].Width, spotlights[7].Height),
                    null, Color.White, lightRotate[7], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[8], new Rectangle(300, -10, spotlights[0].Width, spotlights[8].Height),
                    null, Color.White, lightRotate[8], Vector2.Zero, SpriteEffects.None, 0f);
                sb.End();

                gd.SetRenderTarget(scene);
                gd.Clear(Color.Black);
                sb.Begin();
                DrawCall(sb);
                sb.End();

                gd.SetRenderTarget(null);
                gd.Clear(Color.Black);
                sb.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                pauseFilter.Parameters["filter"].SetValue(light);
                pauseFilter.CurrentTechnique.Passes[0].Apply();
                sb.Draw(scene, Vector2.Zero, Color.White);
                sb.End();
            }
            else
            {
                gd.SetRenderTarget(light);
                gd.Clear(new Color(155, 155, 155));
                sb.Begin(SpriteSortMode.Immediate, BlendState.Additive);
                sb.Draw(spotlights[1], new Rectangle(575, -10, spotlights[1].Width, spotlights[1].Height),
                    null, Color.White, -0.4f, Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[1], new Rectangle(700, -10, spotlights[1].Width, spotlights[1].Height),
                    null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[0], new Rectangle(100, -10, spotlights[0].Width, spotlights[0].Height),
                    null, Color.White, lightRotate[0], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[2], new Rectangle(200, -10, spotlights[0].Width, spotlights[2].Height),
                    null, Color.White, lightRotate[2], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[3], new Rectangle(50, -10, spotlights[0].Width, spotlights[3].Height),
                    null, Color.White, lightRotate[3], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[4], new Rectangle(475, -10, spotlights[0].Width, spotlights[4].Height),
                    null, Color.White, lightRotate[4], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[5], new Rectangle(325, -10, spotlights[0].Width, spotlights[5].Height),
                    null, Color.White, lightRotate[5], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[6], new Rectangle(550, -10, spotlights[0].Width, spotlights[6].Height),
                    null, Color.White, lightRotate[6], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[7], new Rectangle(400, -10, spotlights[0].Width, spotlights[7].Height),
                    null, Color.White, lightRotate[7], Vector2.Zero, SpriteEffects.None, 0f);
                sb.Draw(spotlights[8], new Rectangle(300, -10, spotlights[0].Width, spotlights[8].Height),
                    null, Color.White, lightRotate[8], Vector2.Zero, SpriteEffects.None, 0f);
                sb.End();

                gd.SetRenderTarget(scene);
                gd.Clear(Color.Black);
                sb.Begin();
                DrawCall(sb);
                sb.End();

                gd.SetRenderTarget(null);
                gd.Clear(Color.Black);
                sb.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                pauseFilter.Parameters["filter"].SetValue(light);
                pauseFilter.CurrentTechnique.Passes[0].Apply();
                sb.Draw(scene, Vector2.Zero, Color.White);
                sb.End();
            }
        }

        public void DrawCall(SpriteBatch sb)
        {
            sb.Draw(Lib.background, Vector2.Zero, Color.White);
            sb.Draw(rafters, Vector2.Zero, Color.White);
            sb.Draw(stage, Vector2.Zero, Color.White);
            sb.Draw(rhythmBar, Vector2.Zero, Color.White);

            if (activeWave)
            {
                if (activeWaveStep == 1)
                {
                    sb.Draw(waves[0], Vector2.Zero, Color.White);
                    activeWaveStep++;
                }
                else if (activeWaveStep == 2)
                {
                    sb.Draw(waves[1], Vector2.Zero, Color.White);
                    activeWaveStep++;
                }
                else if (activeWaveStep == 3)
                {
                    sb.Draw(waves[2], Vector2.Zero, Color.White);
                    activeWaveStep++;
                }
                else if (activeWaveStep == 4)
                {
                    sb.Draw(waves[3], Vector2.Zero, Color.White);
                    activeWaveStep++;
                }
                else if (activeWaveStep == 5)
                {
                    sb.Draw(waves[4], Vector2.Zero, Color.White);
                    activeWaveStep++;
                }
                else if (activeWaveStep == 6)
                {
                    sb.Draw(waves[5], Vector2.Zero, Color.White);
                    activeWaveStep = 1;
                    activeWave = false;
                }
            }

            //draws people behind crowd
            for (int i = 0; i < persons.Count; i++)
            {
                sb.Draw(persons[i].Image, new Vector2(persons[i].xPosition, persons[i].yPosition), Color.White);
            }
            #region Draw Right Cues
            if (hitRight)
            {
                sb.Draw(flash[1], new Vector2(460, 20), Color.White);
            }
            else
            {
                sb.Draw(flash[1], new Vector2(460, 20), Color.White * 0.0f);
            }
            for (int v = 0; v < btnsRight.Count; v++)
            {
                sb.Draw(Lib.buttonCue[1], btnsRight[v].pos, Color.White);
            }
            for (int m = 0; m < missedRight.Count; m++)
            {
                sb.Draw(Lib.buttonCue[1], missedRight[m].pos, Color.White);
            }
            #endregion

            #region Draw Left Cues
            if (hitLeft)
            {
                sb.Draw(flash[0], new Vector2(285, 20), Color.White);
            }
            else
            {
                sb.Draw(flash[0], new Vector2(285, 20), Color.White * 0.0f);
            }
            for (int v = 0; v < btnsLeft.Count; v++)
            {
                sb.Draw(Lib.buttonCue[0], btnsLeft[v].pos, Color.White);
            }
            for (int m = 0; m < missedLeft.Count; m++)
            {
                sb.Draw(Lib.buttonCue[0], missedLeft[m].pos, Color.White);
            }
            #endregion
            sb.Draw(rhythmLogo, Vector2.Zero, Color.White);
            //draw crowd back
            sb.Draw(crowd[0].Image, new Vector2(crowd[0].xPosition, crowd[0].yPosition), Color.White);
            //draw crowd front
            sb.Draw(crowd[1].Image, new Vector2(crowd[1].xPosition, crowd[1].yPosition), Color.White);

            if (Lib.lost)
            {
                sb.Draw(over, Vector2.Zero, Color.White);
            }
            sb.DrawString(Lib.gameFont, "Score: " + Lib.score.ToString(), new Vector2(550, 5), Color.Red);
            sb.DrawString(Lib.gameFont, "Multiplier: " + Lib.multiplier.ToString(), new Vector2(100, 5), Color.Red);
        }

        private void bounceCrowd(Person p)
        {
            switch (p.yPosition)
            {
                case (0):
                    {
                        p.yPosition += 10;
                        break;
                    }
                case (125):
                    {
                        p.yPosition += 10;
                        break;
                    }
                case (10):
                    {
                        p.yPosition -= 10;
                        break;
                    }
                case (135):
                    {
                        p.yPosition -= 10;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
        }

        private void pushBack()
        {
            kbCount++;
            if (persons.Count > 0)
            {
                for (int p = 0; p < persons.Count; p++)
                {
                    kb = (int)((int)(300) / (persons[p].Mass + (int)persons[p].Speed));
                    if (kb == 0)
                    {
                        kb = 1;
                    }
                    persons[p].xPosition -= kb;
                    if (persons[p].xPosition == -50)
                    {
                        Lib.score += (ulong)(435216);
                        persons.RemoveAt(p);
                        crowdSize--;
                    }
                }
            }
            kb = 0;
            if (kbCount == 4)
            {
                kb = 20;
            }
            else if (kbCount == 8)
            {
                kb = 40;
                kbCount = 0;
            }
            if (crowd[0].xPosition <= -900)
            {
                Lib.score += (ulong)(4615782);
            }
            else
            {
                crowd[0].xPosition -= kb;
            }
            if (crowd[1].xPosition <= -800)
            {
                Lib.score += (ulong)(4615782);
            }
            else
            {
                crowd[1].xPosition -= kb;
            }
        }

        public void resetPlay()
        {
            btnsRight.Clear();
        }
    }
}
