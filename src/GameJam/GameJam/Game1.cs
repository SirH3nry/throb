using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region State Vars
        public static Game1 thisGame { get; private set; }
        private PlayClass playClass;
        private SongSelect songSelect;
        private MainMenuClass mainMenuClass;
        private OptionsClass optionsClass = new OptionsClass();
        private GameOverClass gameOverClass = new GameOverClass();
        #endregion

        #region Music

        private MusicClass musicClass = new MusicClass();

        #endregion

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 350;
            thisGame = this;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //Set Initial state to main menu
            Lib.gameState = Lib.GameState.Start_Menu;
            mainMenuClass = new MainMenuClass(musicClass);
            playClass = new PlayClass(musicClass);

            playClass.Initialize(GraphicsDevice);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Lib.gameFont = Content.Load<SpriteFont>("gameFont");

            musicClass.LoadContent(Content);
            mainMenuClass.LoadContent(Content);
            playClass.LoadContent(Content);
            gameOverClass.LoadContent(Content);
            songSelect = new SongSelect(musicClass);
            songSelect.LoadContent(Content);
            optionsClass.LoadContent(Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            //update based on state
            switch (Lib.gameState)
            {
                case Lib.GameState.Start_Menu:
                {
                    mainMenuClass.Update(gameTime);
                    //beatCount += gameTime.ElapsedGameTime.TotalSeconds;
                    //if (beatCount >= beatTimer)
                    //{
                    //    beatCount = 0;
                    //    musicClass.playSoundFx();
                    //}
                    break;
                }
                case Lib.GameState.Song_Select:
                {
                    songSelect.Update(gameTime);
                    break;
                }
                case Lib.GameState.Options:
                {
                    optionsClass.Update(gameTime);
                    break;
                }
                case Lib.GameState.Play:
                {
                    playClass.Update(gameTime);
                    break;
                }
                case Lib.GameState.Game_Over:
                {
                    gameOverClass.Update(gameTime);
                    break;
                }
                default:
                {

                    break;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            //draw based on game state
            switch (Lib.gameState)
            {
                case Lib.GameState.Start_Menu:
                    {
                        mainMenuClass.Draw(spriteBatch);
                        break;
                    }
                case Lib.GameState.Song_Select:
                    {
                        songSelect.Draw(spriteBatch);
                        break;
                    }
                case Lib.GameState.Options:
                    {
                        optionsClass.Draw(spriteBatch);
                        break;
                    }
                case Lib.GameState.Play:
                    {
                        playClass.Draw(spriteBatch, GraphicsDevice);
                        break;
                    }
                case Lib.GameState.Game_Over:
                    {
                        gameOverClass.Draw(spriteBatch, GraphicsDevice);
                        break;
                    }
                default:
                    {

                        break;
                    }
            }

            base.Draw(gameTime);
        }

        public void reset()
        {
            Lib.gameOverType = false;
            Lib.numDefeated = 0;
            Lib.score = 0;
            Lib.lost = false;
            Lib.pause = false;
            Lib.deadCount = 0.0;
            Lib.multiplier = 1;
            UnloadContent();
            playClass.resetPlay();
            Initialize();
            LoadContent();
        }
    }
}
