﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{

    public struct Track
    {
        public Song track;
        public string trackName;
        public byte BPM;
    }

    public class MusicClass
    {
        private SoundEffect beat;
        private List<Track> tracks = new List<Track>();
        private Track tempTrack;
        private Song intro_music;

        public MusicClass() { }

        public void LoadContent(ContentManager c)
        {
            beat = c.Load<SoundEffect>("Sound/BumpLumpAmp10");
            intro_music = c.Load<Song>("Sound/Intro");
            #region load Tracks
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/SirH3nry - Anginus Ep2");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 140;
            tracks.Add(tempTrack);
            //-----------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Drake - I'm On One (Glitch & Wobblefish Remix)");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 128;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/dtox - Insert Witty Song Title");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 128;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Ice MC - Think About The Way (Wobblefish Dubstep Remix)");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 140;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Ricethin - Nice Night to Dream");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 99;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Ricethin - The Wind and the Trees");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 140;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Wobblefish - Bass Colours");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 140;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Wobblefish - Blue Moon");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 140;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Wobblefish - Fragma's Miracle");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 136;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Wobblefish - Hot Minute");
            tempTrack.trackName = tempTrack.track.Name;
            tempTrack.BPM = 128;
            tracks.Add(tempTrack);
            //-------------------
            tempTrack = new Track();
            tempTrack.track = c.Load<Song>("Sound/Wobblefish - Shifter");
            tempTrack.trackName = tempTrack.track.Name;
            //tempTrack.artist = tempTrack.track.Artist.ToString();
            tempTrack.BPM = 164;
            tracks.Add(tempTrack);
            //-------------------

            #endregion
        }

        public int getBPM(int index)
        {
            return tracks[index].BPM;
        }

        public double calcBeatsS(int index)
        {
            return (double)(tracks[index].BPM) / 60.0;
        }

        public void playIntroMusic()
        {
            MediaPlayer.IsRepeating = false;
            MediaPlayer.Play(intro_music);
        }

        public void playTrack(int index, bool loop)
        {
            MediaPlayer.IsRepeating = loop;
            MediaPlayer.Volume = (float)(0.5) * (100 / Lib.musicVolume);
            MediaPlayer.Play(tracks[index].track);
        }

        public void stopMusic()
        {
            MediaPlayer.Stop();
        }

        public void playSoundFx()
        {
            beat.Play(1.0f, 0.0f, 0.0f);
        }

        public void PauseTrack()
        {
            MediaPlayer.Pause();
        }

        public void ResumeTrack()
        {
            MediaPlayer.Resume();
        }

        //Used by Song Select
        public string getTrackName(int index)
        {
            return tracks[index].trackName;
        }

        public MediaState getEndSong()
        {
            return MediaPlayer.State;
        }
    }
}
