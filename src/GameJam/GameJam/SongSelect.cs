using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameJam
{
    public class SongSelect
    {

        #region Image Vars

        private Texture2D rafters;
        private Texture2D stage;
        private Texture2D cursor;

        #endregion

        #region String Values
        private String Name1 = "";
        private String Name2 = "";
        private String Name3 = "";
        private String BPM1 = "";
        private String BPM2 = "";
        private String BPM3 = "";

        private String disp1 = "";
        private String disp2 = "";
        private String disp3 = "";
        #endregion

        private int songCount = 1;

        private MusicClass mc;

        public SongSelect(MusicClass mc)
        {
            this.mc = mc;
        }

        public void Initialize(GraphicsDevice gd)
        {

        }

        public void LoadContent(ContentManager c)
        {
            stage = c.Load<Texture2D>("Images/drumSetandStage");
            rafters = c.Load<Texture2D>("Images/rafters");
            cursor = c.Load<Texture2D>("Images/songCursor");
        }

        public void Unload()
        {

        }

        public void Update(GameTime gt)
        {
            Lib.input.Update();
            //NOTE: if up is pressed on song 1 or down is pressed on song 9, it should loop around to the other one
#if WINDOWS
                if ((Lib.input.IsNewPress(Keys.Down)) || (Lib.input.IsNewPress(Keys.S)) ||
                    (Lib.input.IsNewPress(Buttons.LeftThumbstickDown)) || (Lib.input.IsNewPress(Buttons.DPadDown)))
                {

                    songCount = songCount + 1;
                    if (songCount > 10)
                    {
                        songCount = 0;
                    }
                }
                else if ((Lib.input.IsNewPress(Keys.Up)) || (Lib.input.IsNewPress(Keys.W)) ||
                        (Lib.input.IsNewPress(Buttons.LeftThumbstickUp)) || (Lib.input.IsNewPress(Buttons.DPadUp)))
                {
                    songCount = songCount - 1;
                    if (songCount < 0)
                    {
                        songCount = 10;
                    }
                }
                else if ((Lib.input.IsNewPress(Keys.E)) || (Lib.input.IsNewPress(Buttons.RightTrigger)))
                {
                    //Game1.thisGame.reset();
                    Lib.trackSelected = songCount;
                    Lib.gameState = Lib.GameState.Play;
                }
                else if ((Lib.input.IsNewPress(Keys.Q)) || (Lib.input.IsNewPress(Buttons.LeftTrigger)))
                {
                    //Go back to main menu;
                    Lib.gameState = Lib.GameState.Start_Menu;
                }

                #region Update Strings
                if (songCount <= 2)
                {
                    Name1 = mc.getTrackName(0).Substring(6);
                    Name2 = mc.getTrackName(1).Substring(6);
                    Name3 = mc.getTrackName(2).Substring(6);

                    BPM1 = mc.getBPM(0).ToString();
                    BPM2 = mc.getBPM(1).ToString();
                    BPM3 = mc.getBPM(2).ToString();


                    disp1 = Name1 + " - " + BPM1 + " BPM";
                    disp2 = Name2 + " - " + BPM2 + " BPM";
                    disp3 = Name3 + " - " + BPM3 + " BPM";
                }
                else if (songCount <= 5)
                {
                    Name1 = mc.getTrackName(3).Substring(6);
                    Name2 = mc.getTrackName(4).Substring(6);
                    Name3 = mc.getTrackName(5).Substring(6);

                    BPM1 = mc.getBPM(3).ToString();
                    BPM2 = mc.getBPM(4).ToString();
                    BPM3 = mc.getBPM(5).ToString();


                    disp1 = Name1 + " - " + BPM1 + " BPM";
                    disp2 = Name2 + " - " + BPM2 + " BPM";
                    disp3 = Name3 + " - " + BPM3 + " BPM";
                }
                else if (songCount <= 8)
                {

                    Name1 = mc.getTrackName(6).Substring(6);
                    Name2 = mc.getTrackName(7).Substring(6);
                    Name3 = mc.getTrackName(8).Substring(6);

                    BPM1 = mc.getBPM(6).ToString();
                    BPM2 = mc.getBPM(7).ToString();
                    BPM3 = mc.getBPM(8).ToString();


                    disp1 = Name1 + " - " + BPM1 + " BPM";
                    disp2 = Name2 + " - " + BPM2 + " BPM";
                    disp3 = Name3 + " - " + BPM3 + " BPM";
                }
                else if (songCount <= 11)
                {
                    Name1 = mc.getTrackName(9).Substring(6);
                    Name2 = mc.getTrackName(10).Substring(6);

                    BPM1 = mc.getBPM(9).ToString();
                    BPM2 = mc.getBPM(10).ToString();


                    disp1 = Name1 + " - " + BPM1 + " BPM";
                    disp2 = Name2 + " - " + BPM2 + " BPM";
                    disp3 = "";
                }
#endregion

#endif

#if XBOX360
                //insert 360 code here.  
#endif
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Begin();
            sb.Draw(Lib.background, Vector2.Zero, Color.White);
            sb.Draw(rafters, Vector2.Zero, Color.White);
            sb.DrawString(Lib.gameFont, "Song Select", new Vector2(350, 30), Color.White);
            sb.DrawString(Lib.gameFont, disp1, new Vector2(150, 80), Color.White);
            sb.DrawString(Lib.gameFont, disp2, new Vector2(150, 130), Color.White);
            sb.DrawString(Lib.gameFont, disp3, new Vector2(150, 180), Color.White);

            //cursor next to current item
            if (songCount == 0 || songCount == 3 || songCount == 6 || songCount == 9)
            {
                sb.Draw(cursor, new Vector2(100, 80), Color.White);
            }
            else if (songCount == 1 || songCount == 4 || songCount == 7 || songCount == 10)
            {
                sb.Draw(cursor, new Vector2(100, 130), Color.White);
            }
            else if (songCount == 2 || songCount == 5 || songCount == 8)
            {
                sb.Draw(cursor, new Vector2(100, 180), Color.White);
            }
            //simply commented out this part.  Either exit upon song selection, or with an exit
            //sb.DrawString(Lib.gameFont, "Exit", new Vector2(300, 230), Color.White);

            //We need a curser for use on this.


            sb.End();
        }
    }
}