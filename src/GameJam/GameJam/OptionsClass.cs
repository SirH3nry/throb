﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class OptionsClass
    {
        #region Controls

        //bracket coordinates
        int curX = 78;
        int curY = 139;
        //music vol node coordinates
        int musX = 190;
        int musY = 217;
        //sfx vol node coordinates
        int sfxX = 190;
        int sfxY = 291;

        //controls the credits screen
        private bool creds = false;

        //controls the mode arrows(for visual reasons)
        private bool modes = true;
        #endregion

        #region Image Vars

        public Texture2D audioBackground;
        public Texture2D modeBackground;
        public Texture2D mode2Background;
        public Texture2D creditsBackground;
        public Texture2D credits;
        public Texture2D brackets;
        public Texture2D slider;

        #endregion


        public OptionsClass() { }

        public void Initialize()
        {
            //Options theme?
        }

        public void LoadContent(ContentManager c)
        {
            audioBackground = c.Load<Texture2D>("Images/Options/OptionsScreenAudio");
            modeBackground = c.Load<Texture2D>("Images/Options/OptionsScreenMode");
            mode2Background = c.Load<Texture2D>("Images/Options/OptionsScreenMode2");
            creditsBackground = c.Load<Texture2D>("Images/Options/OptionsScreen");
            credits = c.Load<Texture2D>("Images/Options/Credits");
            brackets = c.Load<Texture2D>("Images/Options/selectionNotes");
            slider = c.Load<Texture2D>("Images/Options/sliderNode");
        }

        public void Unload()
        {

        }

        public void Update(GameTime gt)
        {
            Lib.input.Update();
            //change curX and curY just likein MainMenu

            //if selection button on exit, return to main menu
            //if selection button on something else, button press causes option change
#if WINDOWS
                //lefts and rights
                if ((Lib.input.IsNewPress(Keys.Left)) || (Lib.input.IsNewPress(Keys.A)) || 
                    (Lib.input.IsNewPress(Buttons.LeftThumbstickLeft)) || (Lib.input.IsNewPress(Buttons.DPadLeft)))
                {
                    updateBrackets(true, false);
                }
                else if ((Lib.input.IsNewPress(Keys.Right)) || (Lib.input.IsNewPress(Keys.D)) ||
                    (Lib.input.IsNewPress(Buttons.LeftThumbstickRight)) || (Lib.input.IsNewPress(Buttons.DPadRight)))
                {
                    updateBrackets(false, false);
                }
                //ups and downs
                if ((Lib.input.IsNewPress(Keys.Down)) || (Lib.input.IsNewPress(Keys.S)) ||
                    (Lib.input.IsNewPress(Buttons.LeftThumbstickDown)) || (Lib.input.IsNewPress(Buttons.DPadDown)))
                {
                    updateBrackets(true, true);
                }
                else if ((Lib.input.IsNewPress(Keys.Up)) || (Lib.input.IsNewPress(Keys.W)) ||
                    (Lib.input.IsNewPress(Buttons.LeftThumbstickUp)) || (Lib.input.IsNewPress(Buttons.DPadUp)))
                {
                    updateBrackets(false, true);
                }
                //right trigger
                if ((Lib.input.IsNewPress(Keys.E)) || (Lib.input.IsNewPress(Buttons.RightTrigger)))
                {
                    changeState();
                }
                //left trigger
                if ((Lib.input.IsNewPress(Keys.Q)) || (Lib.input.IsNewPress(Buttons.LeftTrigger)))
                {
                    reverseState();
                }
#endif

#if XBOX360
                //insert 360 code here.  
#endif
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Begin();
            if (curX == 78)
            {
                sb.Draw(audioBackground, Vector2.Zero, Color.White);
                sb.Draw(slider, new Vector2(musX, musY), Color.White);
                sb.Draw(slider, new Vector2(sfxX, sfxY), Color.White);
            }
            else if (curX == 335)
            {
                if (modes)
                {
                    sb.Draw(modeBackground, Vector2.Zero, Color.White);
                }
                else
                {
                    sb.Draw(mode2Background, Vector2.Zero, Color.White);
                }

            }
            else if (curX == 579)
            {
                if (creds)
                {
                    sb.Draw(credits, Vector2.Zero, Color.White);
                }
                else
                {
                    sb.Draw(creditsBackground, Vector2.Zero, Color.White);
                }
            }
            if (!creds)
            {
                sb.Draw(brackets, new Vector2(curX, curY), Color.White);

            }

            sb.End();
        }

        //direction = true for left
        //          = false for right
        //isVertical makes direction = true for down
        //                           = false for up
        private void updateBrackets(bool direction, bool isVertical)
        {
            if (!creds)
            {
                //LEFT
                if (direction && !isVertical)
                {
                    if (curX == 78)
                    {
                        if (curY == 139)
                        {
                            curX = 579;
                        }
                        else if (curY == 210)
                        {
                            //adjust music volume negatively
                            if (musX > 90)
                            {
                                musX -= 2;
                                Lib.musicVolume -= 2;
                            }
                        }
                        else if (curY == 281)
                        {
                            //adjust sfx volume negatively
                            if (sfxX > 90)
                            {
                                sfxX -= 2;
                                Lib.sfxVolume -= 2;
                            }
                        }

                    }
                    else if (curX == 335 && curY == 139)
                    {
                        curX = 78;
                    }
                    else if (curX == 579 && curY == 139)
                    {
                        curX = 335;
                    }
                    //mc.playSoundFx();
                }
                //RIGHT
                else if (!direction && !isVertical)
                {
                    if (curX == 78)
                    {
                        if (curY == 139)
                        {
                            curX = 335;
                        }
                        else if (curY == 210)
                        {
                            //adjust music volume positively
                            if (musX < 190)
                            {
                                musX += 2;
                                Lib.musicVolume += 2;
                            }
                        }
                        else if (curY == 281)
                        {
                            //adjust sfx volume positively
                            if (sfxX < 190)
                            {
                                sfxX += 2;
                                Lib.sfxVolume += 2;
                            }
                        }
                    }
                    else if (curX == 335 && curY == 139)
                    {
                        curX = 579;
                    }
                    else if (curX == 579 && curY == 139)
                    {
                        curX = 78;
                    }
                    //mc.playSoundFx();
                }

                //DOWN
                if (direction && isVertical)
                {
                    if (curX == 78)
                    {
                        if (curY == 139)
                        {
                            curY = 210;
                        }
                        else if (curY == 210)
                        {
                            curY = 281;
                        }
                        else if (curY == 281)
                        {
                            curY = 139;
                        }
                    }
                    else if (curX == 335)
                    {
                        if (curY == 139)
                        {
                            curY = 230;
                        }
                        else
                        {
                            curY = 139;
                        }
                    }

                }
                //UP
                if (!direction && isVertical)
                {
                    if (curX == 78)
                    {
                        if (curY == 139)
                        {
                            curY = 281;
                        }
                        else if (curY == 210)
                        {
                            curY = 139;
                        }
                        else if (curY == 281)
                        {
                            curY = 210;
                        }
                    }
                    else if (curX == 335)
                    {
                        if (curY == 139)
                        {
                            curY = 230;
                        }
                        else
                        {
                            curY = 139;
                        }
                    }

                }
            }
        }

        //function to determine the current position and
        //change state accordingly
        private void changeState()
        {
            if (curX == 335 && curY == 230)
            {
                //switch gamemodes
                if (modes)
                {
                    modes = false;
                }
                else
                {
                    modes = true;
                }
            }
            else if (curX == 579)
            {
                creds = true;
            }
        }

        private void reverseState()
        {
            if (creds)
            {
                creds = false;
            }
            else
            {
                Lib.gameState = Lib.GameState.Start_Menu;
            }


        }
    }
}