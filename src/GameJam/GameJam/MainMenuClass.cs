﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class MainMenuClass
    {

        #region Controls
        //bracket coordinates, used to move cursor.
        private int curX = 30;
        private int curY = 57;

        private MusicClass mc;

        #endregion

        #region Images Var
        private Texture2D menu;
        private Texture2D brackets;
        #endregion


        public MainMenuClass(MusicClass mc)
        {
            this.mc = mc;
        }

        public void Initialize()
        {

        }

        public void LoadContent(ContentManager c)
        {
            Lib.background = c.Load<Texture2D>("Images/backgroundMain");
            menu = c.Load<Texture2D>("Images/MenuScreen");
            brackets = c.Load<Texture2D>("Images/Notes");

            mc.playIntroMusic();
        }

        public void Unload()
        {

        }

        public void Update(GameTime gt)
        {
            Lib.input.Update();

#if WINDOWS

            if ((Lib.input.IsNewPress(Keys.Left)) || (Lib.input.IsNewPress(Keys.A)) ||
                (Lib.input.IsNewPress(Buttons.LeftThumbstickLeft)) || (Lib.input.IsNewPress(Buttons.DPadLeft)))
            {
                updateBrackets(true);
            }
            else if ((Lib.input.IsNewPress(Keys.Right)) || (Lib.input.IsNewPress(Keys.D)) ||
                (Lib.input.IsNewPress(Buttons.LeftThumbstickRight)) || (Lib.input.IsNewPress(Buttons.DPadRight)))
            {
                updateBrackets(false);
            }
            if ((Lib.input.IsNewPress(Keys.E)) || (Lib.input.IsNewPress(Keys.Space)) ||
                (Lib.input.IsNewPress(Buttons.RightTrigger)) || (Lib.input.IsNewPress(Buttons.Start)))
            {
                changeState();
            }

#endif

#if XBOX360
                //insert 360 code here.  
#endif
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Begin();
            sb.Draw(menu, Vector2.Zero, Color.White);
            sb.Draw(brackets, new Vector2(curX, curY), Color.White);
            sb.End();
        }

        //direction = true for left
        //          = false for right
        private void updateBrackets(bool direction)
        {
            if (direction)
            {
                if (curX == 30)
                {
                    curX = 280;
                }
                else if (curX == 150)
                {
                    curX = 30;
                }
                else if (curX == 280)
                {
                    curX = 150;
                }
                mc.playSoundFx();
            }
            else
            {
                if (curX == 30)
                {
                    curX = 150;
                }
                else if (curX == 150)
                {
                    curX = 280;
                }
                else if (curX == 280)
                {
                    curX = 30;
                }
                mc.playSoundFx();
            }
        }

        //function to determine the current position and
        //change state accordingly
        private void changeState()
        {
            if (curX == 30)
            {
                mc.stopMusic();
                Lib.gameState = Lib.GameState.Song_Select;
            }
            else if (curX == 150)
            {
                mc.stopMusic();
                Lib.gameState = Lib.GameState.Options;
            }
            else if (curX == 280)
            {
                Game1.thisGame.Exit();
            }
        }
    }
}
