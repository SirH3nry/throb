﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class Person
    {
        private Texture2D image;
        private int xPos, yPos;
        private double speed;
        private int mass;

        public Person() { }

        public Person(double speed, int mass)
        {
            this.speed = speed;
            this.mass = mass;
        }

        public Texture2D Image
        {
            get { return image; }
            set { image = value; }
        }

        public int xPosition
        {
            get { return xPos; }
            set { xPos = value; }
        }

        public int yPosition
        {
            get { return yPos; }
            set { yPos = value; }
        }

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public int Mass
        {
            get { return mass; }
            set { mass = value; }
        }
    }
}
