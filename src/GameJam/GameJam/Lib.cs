﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace GameJam
{
    public static class Lib
    {
        public enum GameState
        {
            Start_Menu,
            Song_Select,
            Options,
            Play,
            Game_Over
        }

        public static InputHelper input = new InputHelper();

        public static GameState gameState;
        public static SpriteFont gameFont;
        public static int trackSelected = 1;
        public static bool pause = false;
        public static bool lost = false;

        public static bool gameOverType = false;
        public static double deadCount = 0.0;
        public static readonly double deadTimer = 2.5;
        public static ulong score = 0;
        public static ulong numDefeated = 0;
        public static double heartRate = 60;
        public static int multiplier = 1;
        public static int musicVolume = 100;
        public static int sfxVolume = 100;

        #region Multi-use Gfx

        public static Texture2D background;
        //0 - left
        //1 - right
        public static Texture2D[] buttonCue = new Texture2D[2];
        public static Texture2D[] people = new Texture2D[4];

        #endregion
    }
}
