﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class ScrollButton
    {
        public double scrollTime;
        public double scrollCount;
        public Vector2 pos;

        //;start = true if right, false if left
        public ScrollButton(bool start)
        {
            scrollCount = 0.0;
            scrollTime = 6.0;
            if (start)
            {
                pos = new Vector2(750, 33);
            }
            else
            {
                pos = new Vector2(50, 33);
            }
        }
    }
}
