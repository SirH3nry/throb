sampler s0;
texture filter;
sampler pauseFilter = sampler_state{Texture = filter;};

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0  
{  
    float4 color = tex2D(s0, coords);  
    float4 lightColor = tex2D(pauseFilter, coords);  
    return color * lightColor;  
} 

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
